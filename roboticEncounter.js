///////////////////////////////////////////////////////////////////////////////////
//////////////////////////     Robotic Encounter    ///////////////////////////////
////////////////////   Made by: Paudric Smith & Adam Zieba   //////////////////////
///////////////////////////////////////////////////////////////////////////////////

// Waits for everything to load before displaying
window.onload = function() {


	var requestAnimationFrame = (function() {
		return window.requestAnimationFrame ||
			window.webkitRequestAnimationFrame ||
			window.mozRequestAnimationFrame ||
			window.oRequestAnimationFrame ||
			window.msRequestAnimationFrame ||
			function(callback, element) {
				window.setTimeout(callback, 1000 / 60);
			};
	})();

	/////////////////////////////////////////////////////////////////////////////////
	//////////////////////////     Canvas/Context    ///////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////
	var canvas = document.getElementById('roboticEncounter-canvas');
	var ctx = canvas.getContext('2d');
	var WIDTH = 800;
	var HEIGHT = 500;
	var gameStartTime = Date.now(); //return time in milliseconds
	ctx.mozImageSmoothingEnabled = false; //better graphics for pixel art
	ctx.msImageSmoothingEnabled = false;
	ctx.imageSmoothingEnabled = false;

	var Img = {};
	Img.player = new Image();
	Img.player.src = "media/spritesheet/playerGun.png";
	Img.bullet = new Image();
	Img.bullet.src = 'media/images/bullet.png';


	//////////////////////////////////////////////////////////////////////////////////
	//////////////////////////     Game Variables    ////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////
	var fps = document.getElementById('fps');
	var startTime = Date.now();
	var frame = 0;

	var gameStarted = false;
	var frameCounter = 0;
	var score = 0;
	var isPaused = false;




	/////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////     Functions    /////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////
	// test collision function ---------------------------------------------------------------
	testCollision = function(collisionObjOne, collisionObjTwo) {
		return collisionObjOne.x <= collisionObjTwo.x + collisionObjTwo.width &&
			collisionObjTwo.x <= collisionObjOne.x + collisionObjOne.width &&
			collisionObjOne.y <= collisionObjTwo.y + collisionObjTwo.height &&
			collisionObjTwo.y <= collisionObjOne.y + collisionObjOne.height;
	}


	function fpsCounter() {
		var time = Date.now();
		console.log('Inside fpsCounter function');
		frame++;
		if (time - startTime > 1000) {
			fps.innerHTML = (frame / ((time - startTime) / 1000)).toFixed(0);
			startTime = time;
			frame = 0;
		}
		requestAnimationFrame(fpsCounter);
	}
	fpsCounter();


	// Prints to the console a message in red for debugging 
	function colorTrace(msg, color) {
		console.log("%c" + msg, "color:" + color + ";font-weight:bold;");
	}




	function introScreen() {

		console.log('Inside introScreen');

		clearCanvas();
		ctx.fillStyle = "#000000";
		ctx.fillRect(0, 0, canvas.width, canvas.height);
		ctx.font = "50px Arial";
		ctx.textAlign = "center";

		ctx.fillStyle = "#03CDFF";
		ctx.font = "20px Arial";
		ctx.fillText("Keyboard:", canvas.width / 2, canvas.height / 2 - 40);
		ctx.font = "15px Arial";
		ctx.fillText("Move left / right - left / right arrow keys or A / D", canvas.width / 2, canvas.height / 2 + 10);
		ctx.fillText("Move up / down - up / down arrow keys or W / S", canvas.width / 2, canvas.height / 2 + 40);


		ctx.font = "20px Arial";
		ctx.fillText("Press Enter To Start", canvas.width / 2, canvas.height - 70);

		if (!gameStarted) {
			requestAnimationFrame(introScreen);
		} else {
			requestAnimationFrame(gameLoop);
		}

	}



	////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////    Main game loop function   //////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////

	document.onkeydown = function(e) {
		if (e.keyCode === 68) //d
			player.rightPressed = true;
		else if (e.keyCode === 83) //s
			player.downPressed = true;
		else if (e.keyCode === 65) //a
			player.leftPressed = true;
		else if (e.keyCode === 87) // w
			player.upPressed = true;

		else if (e.keyCode === 80) //p
			isPaused = !isPaused;
	}

	document.onkeyup = function(e) {
		if (e.keyCode === 68) //d
			player.rightPressed = false;
		else if (e.keyCode === 83) //s
			player.downPressed = false;
		else if (e.keyCode === 65) //a
			player.leftPressed = false;
		else if (e.keyCode === 87) // w
			player.upPressed = false;
	}


	update = function() {
		if (isPaused) {
			ctx.fillText('Paused', WIDTH / 2, HEIGHT / 2);
			return;
		}

		ctx.clearRect(0, 0, WIDTH, HEIGHT);
		Maps.current.draw();
		frameCounter++;
		score++;
		player.update();

		ctx.fillText(player.hp + " Hp", 0, 50);
		ctx.fillText('Score: ' + score, 250, 50);
	}

	newGame = function() {
		player.hp = 10;
		gameStartTime = Date.now();
		frameCounter = 0;
		score = 0;

	}

	Maps = function(id, imgSrc, width, height) {
		var self = {
			id: id,
			image: new Image(),
			width: width,
			height: height
		}
		self.image.src = imgSrc;


		self.draw = function() {
			var x = 0 - player.x;
			var y = 250 - player.y;
			ctx.drawImage(self.image, 0, 0, self.image.width, self.image.height, x, y, self.image.width * 2, self.image.height * 2);
		}
		return self;
	}

	Maps.current = Maps('world', 'media/images/world.png', 15600, 780);

	Player = function() {
		var self = Actor('player', 'myId', 50, 40, 75, 105, Img.player, 10, 1);


		var superUpdate = self.update;
		self.update = function() {
			superUpdate();
			if (self.rightPressed || self.leftPressed || self.downPressed || self.upPressed)
				self.animationStep += 0.2;
			if (self.leftMouseButton)
				self.shoot();
			if (self.rightMouseButton)
				self.critical();
		}

		self.updatePosition = function() {
			if (self.rightPressed)
				self.x += 10;
			if (self.leftPressed)
				self.x -= 10;
			if (self.downPressed)
				self.y += 10;
			if (self.upPressed)
				self.y -= 10;

			//ispositionvalid
			if (self.x < self.width / 2)
				self.x = self.width / 2;
			if (self.x > Maps.current.width - self.width / 2)
				self.x = Maps.current.width - self.width / 2;
			if (self.y < self.height / 2)
				self.y = self.height / 2;
			if (self.y > Maps.current.height - self.height / 2)
				self.y = Maps.current.height - self.height / 2;
		}
		self.onDeath = function() {
			var timeSurvived = Date.now() - gameStartTime;
			console.log("You lost! You survived for " + timeSurvived + " ms.");
			newGame();
		}
		self.downPressed = false;
		self.upPressed = false;
		self.leftPressed = false;
		self.rightPressed = false;

		self.leftMouseButton = false;
		self.rightMouseButton = false;

		return self;

	}

	var player;


	Entity = function(type, id, x, y, width, height, img) {
		var self = {
			type: type,
			id: id,
			x: x,
			y: y,
			width: width,
			height: height,
			img: img,
		};
		self.update = function() {
			self.updatePosition();
			self.draw();
		}
		self.draw = function() {
			ctx.save();
			var x = self.x - player.x;
			var y = self.y - player.y;

			x += WIDTH / 2;
			y += HEIGHT / 2;

			x -= self.width / 2;
			y -= self.height / 2;

			ctx.drawImage(self.img,
				0, 0, self.img.width, self.img.height,
				x, y, self.width, self.height
			);

			ctx.restore();
		}
		self.getDistance = function(entity2) { //return distance (number)
			var vx = self.x - entity2.x;
			var vy = self.y - entity2.y;
			return Math.sqrt(vx * vx + vy * vy);
		}

		self.testCollision = function(entity2) { //return if colliding (true/false)
			var collisionObjOne = {
				x: self.x - self.width / 2,
				y: self.y - self.height / 2,
				width: self.width,
				height: self.height,
			}
			var collisionObjTwo = {
				x: entity2.x - entity2.width / 2,
				y: entity2.y - entity2.height / 2,
				width: entity2.width,
				height: entity2.height,
			}
			return testCollision(collisionObjOne, collisionObjTwo);

		}
		self.updatePosition = function() {}

		return self;
	}

	Actor = function(type, id, x, y, width, height, img, hp, attackSpeed) {
		var self = Entity(type, id, x, y, width, height, img);

		self.hp = hp;
		self.hpMax = hp;
		self.attackSpeed = attackSpeed;
		self.attackCounter = 0;
		self.aimAngle = 0;

		self.animationStep = 0;

		self.draw = function() {
			ctx.save();
			var x = self.x - player.x;
			var y = self.y - player.y;

			x += WIDTH / 2;
			y += HEIGHT / 2;

			x -= self.width / 2;
			y -= self.height / 2;

			var frameWidth = self.img.width / 3;
			var frameHeight = self.img.height / 4;

			var aimAngle = self.aimAngle;
			if (aimAngle < 0)
				aimAngle = 360 + aimAngle;

			var directionMod = 2; //draw down
			if (player.downPressed) //down
				directionMod = 2;
			else if (player.leftPressed) //left
				directionMod = 1;
			else if (player.upPressed) //up
				directionMod = 0;
			else if (player.rightPressed)
				directionMod = 3;

			var walkingMod = Math.floor(self.animationStep) % 3; //1,2

			ctx.drawImage(self.img,
				walkingMod * frameWidth, directionMod * frameHeight, frameWidth, frameHeight,
				x, y, self.width, self.height
			);

			ctx.restore();
		}

		var superUpdate = self.update;
		self.update = function() {
			superUpdate();
			self.attackCounter += self.attackSpeed;
			if (self.hp <= 0)
				self.onDeath();
		}
		self.onDeath = function() {};

		self.shoot = function() {
			if (self.attackCounter > 25) { //every 1 sec
				self.attackCounter = 0;
				Bullet.generate(self);
			}
		}

		self.critical = function() {}


		return self;
	}

	player = Player();
	newGame();

	setInterval(update, 40);

	/////////////////////////////////////////////////////////////////////////////////
	//////////////////////////     Start the Game!    ///////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////
	// introScreen();



}