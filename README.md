# CA2_2D_Game_PaudricSmith_AdamZieba
 ---
 ---
## *~A 2d Survival Shooter~*
---
---
# === **Planning** ===
---
- Survival
- Shooter
- Side scroller
=======
### //✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮
### //✮✮✮✮✮✮ CA2_2D_Game_PaudricSmith_AdamZieba ✮✮✮✮✮✮✮✮
### //✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮

# Welcome!
This is our README file where you can find the game's description and reference links.

# BitBucket REPO Link
✮✮✮`https://bitbucket.org/Eaglez98/ca2_2d_paudricsmith_adamzieba/src/master/`✮✮✮

#About
`Name:`  Robotic Encounter
`Genre:` Survival/2D side-scroller/Shooter
`About:` We decided that our game will be a shooter where Robots have taken over the control of the world. There is no hope for humanity and You as a lone-ranger is trying to surviver as long as possible, trying to take out as many robots with you as possible. 
Our aim was to create a fast paced shooter where player's skill is the tool to survival. As you progress forward the difficulty increases but but so does your damage ;) 

# ✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮ **Planning** ✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮

### Shooter psuedo code:
	* shoot something.
	* update score.
	* pick collectibles.
	* increased difficulty as surviving.
	
### Player:
	* Run sprite
	* Shoot sprite for each weapon
	* Health - 100
	* Stamina - has to wait to regain to run again.
	* Walk Speed - 10pxps
	* Run Speed - 10pxps
	* Fist - 1d
	* Weapon - holds it

### Monster:
	* Walk sprite
	* Sprite looks slightly different per min.
	* Speed - 5pxps, then 1++ per min.
	* Health 5
	* Health increases 2 per min
	* Max Health 50

### Shop: (if have time do this) 
	* Tap door to enter new screen
	* 2 things to buy - ammo, food.

### Drops:
	* Dead monsters randomly drop ammo or food.
	* Guns drop randomly on path(if no shop, else buy guns in shop).

### Guns:
	* Gun sprite per gun
	* Pistol - 6b - 5d - 1 in 5 Crit 10d
	* Shotgun - 2b - 10d - 1 in 5 Crit 20d
	* Uzi - 20b - 2.5d - 1 in 5 Crit 5d

### Ammo:
	* Ammo sprite per gun 
	* Pistol ammo generic bullet sprite
	* Shotgun ammo spray sprite
	* Uzi ammo 1/2 pistol sprite 

# ✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮

# Code References 

* 1) We have taken certain bits of code and concepts from our own games (Hell Arena/Jammy Dodger)

# References
We would like to thank to `John Loane` & `Niall McGuinness` for teaching
us certain concepts required to make a 2D game that works on mobile.


RainingChain youtuber videos on JavaScript
*https://www.youtube.com/channel/UC8Yp-YagXZ4C5vOduEhcjRw*

We would also like to reference `Derek Crilly` for certain code structures we have found on his public source website:
*http://derek.dkit.ie/*.