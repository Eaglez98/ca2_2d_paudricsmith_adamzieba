//✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮
//✮✮✮✮✮✮✮✮✮✮ Robotic Encounter ✮✮✮✮✮✮✮✮✮✮✮✮
//✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮

class Monster {
	constructor(x, y, speed) {
		//Initial x,y is outside the canvas on the RHS
		//at random spawn points
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.NUMBER_OF_COLUMNS_IN_SPRITE_IMAGE = 9; //columns in spritesheets
		this.NUMBER_OF_ROWS_IN_SPRITE_IMAGE = 4;	//rows in spritesheet
	}
}